<!doctype html>
<html lang="en">
	<head>
		<title>Welcome to KTCS</title>
		<style>
			body {font-family: "Lato", sans-serif;}
	
			/* Style the tab */
			div.tab {
				overflow: hidden;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
			}
	
			/* Style the buttons inside the tab */
			div.tab button {
				background-color: inherit;
				float: left;
				border: none;
				outline: none;
				cursor: pointer;
				padding: 14px 16px;
				transition: 0.3s;
				font-size: 17px;
				}
		
			/* Change background color of buttons on hover */
			div.tab button:hover {
				background-color: #ddd;
			}
			
			/* Create an active/current tablink class */
			div.tab button.active {
				background-color: #ccc;
			}
			
			/* Style the tab content */
			.tabcontent {
				display: none;
				padding: 6px 12px;
				border: 1px solid #ccc;
				border-top: none;
			}
			
			.tabcontent {
				-webkit-animation: fadeEffect 1s;
				animation: fadeEffect 1s; /* Fading effect takes 1 second */
			}
			
			@-webkit-keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}
			
			@keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}
		</style>
		<script>
		function openAction(evt, actionName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(actionName).style.display = "block";
			evt.currentTarget.actionName += " active";
		}
		</script>
		<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>


	</head>
	<body>
		<?php $defaultTimeZone='UTC'; ?>
		<?php
		include_once '../config/auth.php';
		if($id=checkAuth()){
	
		} else {
			echo("no auth");
			header("Location: /index.php?logout=1");
			die();
		}
		?>
		HELLO! <a href="/index.php?logout=1">Log Out</a><br/>
	
		<?php
		include_once "../config/connect.php";
		$con = connect();
		?>
	
		<div class="tab">
			<button class="tablinks" onclick="openAction(event,'Search')" id="defaultOpen">Search</button>
			<button class="tablinks" onclick="openAction(event, 'My Reservation')">My Reservation</button>
			<button class="tablinks" onclick="openAction(event, 'My Rent')">My Rent</button>
		</div>
		
		<div id="Search" class="tabcontent">
		
		<form name='searchTb' id="searchTb" action="index.php" method='post'>
			From:
			<input type="date" name="bday" value="<?php	if(isset($_POST['bday']))
															echo $_POST['bday'];
														else
															echo date('Y-m-d'); 
														?>">
			<br/>
			End:
			<input type="date" name="eday" value="<?php	if(isset($_POST['eday']))
															echo $_POST['eday'];
														else
															echo date('Y-m-d'); 
														?>">
			<br/>
			Location:
			<select name='location' id='location'>
			<?php
			$result = $con->query("SELECT pAddr FROM PLoc");
			
			if(!$result)
				printf("<option value=\"NA\">NA</option>");
			else{
				while ($row = $result->fetch_assoc()) {
					printf("<option value=\"%s\">%s</option>",$row['pAddr'],$row['pAddr']);
				}
			}
			?>
			</select>
			<br/>
			<input type="submit" id='search' name='search' />
		</form>
		
		<?php
			if(!isset($_POST['search'])) {
				//die();
			} else {
			$beginday = strtotime($_POST["bday"]. ' UTC');
			$endday = strtotime($_POST["eday"]. ' UTC');
			printf("%d</br>",$beginday);
			printf("%d</br>",$endday);
				if ($_POST['eday']-$_POST['bday'] < 0)
					echo "Bad quary. Check the date.<br /> ";
				else {
			$query = "SELECT cVIN, cFee, cMake, cModel, cYear, pAddr FROM Car WHERE pAddr = ?";
			$stmt = $con->prepare($query);
			$stmt->bind_Param("s", $_POST['location']);
			$stmt->execute();
			$stmt->bind_result($vin, $fee, $make, $model, $year, $addr);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				printf("Your search returns no result.<br/>");
				die();
			}
			else{ ?>
		<form name='reserveTb' id="reserveTb" action="reserve.php" method="post">
		<table>
			<tr>
				<th>Select</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Daily Fee</th>
				<th>Address</th>
			</tr>
	 		<?php
			while ($stmt->fetch()) {
				$query2 = "SELECT reDate, reLength FROM reservation WHERE cvin = ? AND (NOT ( reDate > ? OR (reDate + (reLength - 1) * 24 * 60 * 60 < ?)))";
				$stmt2 = $con->prepare($query2);
				$stmt2->bind_Param("sdd",$vin, $endday, $beginday);
				$stmt2->execute();
				$stmt2->bind_result($bday, $len);
				$stmt2->store_result();
				printf("<tr>");
				if ($stmt2->num_rows() != 0) { ?>
					<td>
						<table>
							<tr><td>Reserved</td></tr>
							<?php while($stmt2->fetch()){ ?>
							<tr><td> <?php printf("From %s to %s",date("d/m/Y", $bday), date("d/m/Y", $bday+($len-1)*26*60*60)); ?> </td></tr>
							<?php } ?>
						</table>
					</td>
					<?php
				} else {
					printf("<td><input type=\"radio\" name=\"vin\" id=\"vin\" value=\"%s\">%s</td>",$vin,$vin);
				}
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%d</td>",$fee);
					printf("<td>%s</td>",$addr);
					printf("</tr>");
				} ?>
				</table>
				<input type="hidden" id='bday' name='bday' value="<?php printf("%d",$beginday); ?>">
				<input type="hidden" id='eday' name='eday' value="<?php printf("%d",$endday); ?>">
				<input type="submit" id='reserve' name='reserve' method='post'/>
				</form>
			<?php
			}
		}
			}
		?>
		</div>
		
		<div id="My Reservation" class="tabcontent">
		<?php
			$query = "SELECT reID, reAccCode, cMake, cModel, cYear, cFee, pAddr, reDate, reLength, cvin FROM reservation JOIN car USING(cVIN) WHERE meNum = ?";
			$stmt = $con->prepare($query);
			$stmt->bind_Param("d",$id);
			$stmt->execute();
			$stmt->bind_result($reid, $code, $make, $model, $year, $fee, $addr, $bday, $len, $cvin);
			$stmt->store_result();
		if ($stmt->num_rows() != 0) { ?>
		<form name='pickUpTb' id="pickUpTb" action="pickup.php" method="post">
		<table>
			<tr>
				<th>Select</th>
				<th>Begin Date</th>
				<th>End Date</th>
				<th>Address</th>
				<th>Access Code</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Daily Fee</th>
			</tr>
			<?php while($stmt->fetch()){ ?>
			<tr>
				<?php
				if ($bday <= time())
					if ($bday + $len * 24*60*60 < time())
						continue;
					else
					printf("<td><input type=\"radio\" name=\"reid\" id=\"reid\" value=\"%d\">%d</td>",$reid,$reid);
				else
					printf("<td>%d",$reid);
				?>
				<?php printf("<td>%s</td>",date("d/m/Y", $bday)); ?>
				<?php printf("<td>%s</td>",date("d/m/Y", $bday+($len-1)*26*60*60)); ?>
				<?php printf("<td>%s</td>",$addr); ?>
				<?php printf("<td>%s</td>",$code); ?>
				<?php printf("<td>%s</td>",$make); ?>
				<?php printf("<td>%s</td>",$model); ?>
				<?php printf("<td>%d</td>",$year); ?>
				<?php printf("<td>%d</td>",$fee); ?>
			</tr>
			<?php } ?>
			</table>
		        <div>
					<label for="reading">Odometer Reading :</label>
					<input type="text" name="oread" id="oread" placeholder="Odometer Reading">
					<label for="status">Status :</label>
					<select name='status' id='status'>
					<option value="normal">normal</option>
					<option value="damaged">damaged</option>
					<option value="not_running">not running</option>
		          <input type="submit" data-inline="true" value="Pick Up">
		        </div>
			
			</form>
		<?php
		} else {
			printf("No reservation");
		}
		?>
		</div>
		
		<div id="My Rent" class="tabcontent">
		<?php
			$query = "SELECT rhid, rhpRead, rhpTime, rhpStatus, rhdRead, rhdTime, rhdStatus, cMake, cModel, cYear, cFee, pAddr FROM renthistory JOIN car USING(cVIN) WHERE meNum = ?";
			$stmt = $con->prepare($query);
			$stmt->bind_Param("d",$id);
			$stmt->execute();
			$stmt->bind_result($rhid, $rhpread, $rhptime, $rhpstatus, $rhdread, $rhdtime, $rhdstatus, $make, $model, $year, $fee, $addr);
			$stmt->store_result();
		if ($stmt->num_rows() != 0) { ?>
		<form name='rentHTb' id="rentHTb" action="dropoff.php" method="post">
		<table>
			<tr>
				<th>Select</th>
				<th>Pick Up Date</th>
				<th>Pick Up Reading</th>
				<th>Pick Up State</th>
				<th>Drop Off Date</th>
				<th>Drop Off Reading</th>
				<th>Drop Off Status</th>
				<th>Address</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Daily Fee</th>
			</tr>
			<?php while($stmt->fetch()){ ?>
			<tr>
				<?php
				if ($bday < time())
					if ($rhdtime)
						printf("<td>%d",$reid);//continue;
					else
					printf("<td><input type=\"radio\" name=\"rhid\" id=\"rhid\" value=\"%d\">%d</td>",$rhid,$rhid);
				else
					printf("<td>%d",$reid);
				?>
				<?php printf("<td>%s</td>",date("d/m/Y", $rhptime)); ?>
				<?php printf("<td>%d</td>",$rhpread); ?>
				<?php printf("<td>%s</td>",$rhpstatus); ?>
				<?php printf("<td>%s</td>",$rhdtime?date("d/m/Y", $rhdtime):""); ?>
				<?php printf("<td>%s</td>",$rhdread?$rhdread:""); ?>
				<?php printf("<td>%s</td>",$rhdstatus); ?>
				<?php printf("<td>%s</td>",$addr); ?>
				<?php printf("<td>%s</td>",$make); ?>
				<?php printf("<td>%s</td>",$model); ?>
				<?php printf("<td>%d</td>",$year); ?>
				<?php printf("<td>%d</td>",$fee); ?>
			</tr>
			<?php } ?>
			</table>
		        <div>
		        	<div>
						<label for="reading">Odometer Reading :</label>
						<input type="text" name="oread" id="oread" placeholder="Odometer Reading">
						<label for="status">Status :</label>
						<select name='status' id='status'>
							<option value="normal">normal</option>
							<option value="damaged">damaged</option>
							<option value="not_running">not running</option>
						</select>
					</div>
					<div>
						<label for="rating">Rating :</label>
						<select name='rating' id='rating'>
							<option value="1">1</option>
							<option value="2">2</option>
							<option value="3">3</option>
							<option value="3">4</option>
						</select>
						<label for="comment">Comment :</label>
						<input type="text" name="comment" id="comment" placeholder="Comment">
					</div>
		          <input type="submit" data-inline="true" value="Drop Off">
		        </div>
			
			</form>
		<?php
		} else {
			printf("No rent");
		}
		?>
		</div>
		
		<script>
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
	</body>
</html>
