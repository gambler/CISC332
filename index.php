<!DOCTYPE HTML>
<html>
  <head>
    <title>Welcome to KTCS</title>
  </head>
  <body>

  <?php
    include_once '/config/connect.php';
    $con = connect();
  ?>
  <?php
    if(isset($_GET['logout'])){
      $sel_q = "DELETE FROM Session WHERE sID=?";
      $stmt = $con->prepare($sel_q);
      $stmt->bind_param('s', $_COOKIE["ktcs_auth"]);
      $stmt->execute();
      unset($_COOKIE["ktcs_auth"]);
    }
  ?>
 
  <?php
    include_once('/config/auth.php');
    
    if(checkAuth()) {
      header("Location: /ktcs/index.php?");
      die();
    }
  ?>
 
  <?php
    if(isset($_POST['loginBtn'])){
      $query = "SELECT meNum, mePw, ifAdmin FROM member WHERE meNum=?";
 
      $stmt = $con->prepare($query);
      $stmt->bind_Param("s", $_POST['account']);
      $stmt->execute();
      $result = $stmt->get_result();
      $obj = $result->fetch_object();
      if(!$obj)
        printf("account does not exist");
      else {
      	if ($obj->mePw != md5($_POST['password']))
          printf("wrong password");
      	else {
        	$expTime = time()+600;
        	$sel_q = "SELECT sID FROM session WHERE sID =?";
	        $ins_q = "INSERT INTO session VALUES(?,FROM_UNIXTIME(?),?)";
	        $del_q = "DELETE FROM session WHERE sID=?";
	        
	        do {
	          $rnum = MD5(rand());
	          $stmt = $con->prepare($sel_q);
	          $stmt->bind_param('s',$rnum);
	          $stmt->execute();
	          $result = $stmt->get_result();
	          //print($result->fetch_object());
	          if(!$result)
	            break;
	          else if($result->fetch_object()->sExpTime < time()) {
	            $stmt = $con->prepare($del_q);
	            $stmt->bind_param('s',$rnum);
	            $stmt->execute();
	            break;
	          }
	        } while (true);
	        unset($_COOKIE["ktcs_auth"]);    
	        setcookie("ktcs_auth",$rnum,$expTime);
	        $stmt = $con->prepare($ins_q);
	        $stmt->bind_param('sds',$rnum,$expTime,$_POST['account']);
	        $stmt->execute();
	        $result = $stmt->get_result();
	        printf($rnum);
	        printf("logged in");
	        printf($obj->ifAdmin);
	        if($obj->ifAdmin == 1)
	        	header("Location: /ktcs/admin.php");
	        else 
	        	header("Location: /ktcs/index.php");
	        exit;
	      }
      }
    } 
  ?>

Log in, or <a href="ktcs/signup.php">Sign Up</a><br/>
  <form name='login' id='login' action='index.php' method='post'>
    <table border='0'>
      <tr>
        <td>Username</td>
        <td><input type='text' name='account' id='account' /></td>
      </tr>
      <tr>
        <td>Password</td>
        <td><input type='password' name='password' id='password' /></td>
      </tr>
      <tr>
        <td></td>
        <td>
          <input type='submit' id='loginBtn' name='loginBtn' value='Log In' /> 
        </td>
      </tr>
    </table>
  </form>

</body>
</html>