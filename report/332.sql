prompt --Use session ID to check which member it is, and if the session has expired.
SET @sid = "" + md5(now());
insert into Session values
(@sid, "2017-02-28 17:54:00", 0000000222);
SELECT meNum, UNIX_TIMESTAMP(sExpTime) as expTime FROM Session WHERE sID=(@sid);

prompt --Check if who logged in is an administrator.
SELECT ifAdmin FROM member WHERE meNum=0000000001;

prompt --Show all parking locations
SELECT pAddr FROM PLoc;

prompt --Select all cars at a parking location
SELECT cVIN, cFee, cMake, cModel, cYear, pAddr FROM Car WHERE pAddr = "1011 Albert";

prompt --Given a period of time, show if reservation(s) has been made to a car conflicts to the time.
SELECT reDate, reLength FROM reservation WHERE cvin = 00000000000000001 AND (NOT ( reDate > UNIX_TIMESTAMP("2017-04-10") OR (reDate + (reLength - 1) * 24 * 60 * 60 < UNIX_TIMESTAMP("2017-04-07"))));

prompt --Reserve a car
INSERT INTO reservation (reDate, reAccCode, reLength, meNum, cVIN) VALUES (UNIX_TIMESTAMP("2017-05-01"), "4242424242", 3, 0000000222, "00000000000000002");

prompt --Show all reservations of a member
SELECT reID, reAccCode, cMake, cModel, cYear, cFee, pAddr, reDate, reLength, cvin FROM reservation JOIN car USING(cVIN) WHERE meNum = 0000000111;

prompt --Check if reservation id is valid
SELECT cVIN FROM reservation WHERE reid = 1;

prompt --Pick up a reserved car
INSERT INTO RentHistory (rhPRead, rhPTime, rhPStatus, meNum, cVIN) VALUES (1000, UNIX_TIMESTAMP("2017-04-07"), "normal", 0000000111, "00000000000000001");
DELETE FROM reservation WHERE reid = 4;

prompt --Show all rent of a member, both past and current(still renting)
SELECT rhid, rhpRead, rhpTime, rhpStatus, rhdRead, rhdTime, rhdStatus, cMake, cModel, cYear, cFee, pAddr FROM renthistory JOIN car USING(cVIN) WHERE meNum = 111;

prompt --Drop a car with comment
SELECT cvin FROM renthistory WHERE rhID = 4;
UPDATE RentHistory SET rhdRead = 2000, rhdTime = UNIX_TIMESTAMP("2017-05-03 23:59:59"), rhdStatus = "normal" WHERE rhID = 4;
UPDATE car SET cRead = 2000 WHERE cvin = "00000000000000001";
INSERT INTO comment (coRating, coText, meNum, cVIN) VALUES ("4", "good", 0000000111, "00000000000000001");

prompt --Show all cars that have travelled 5000 kms or more since their last maintenance.
SELECT cVIN, cMake, cModel, cYear, pAddr, cRead FROM car WHERE cRead - 5000 > (SELECT max(maRead) FROM Car JOIN maintenance USING(CVIN) WHERE cvin = 99999999999999999 GROUP BY CVIN);

prompt --Find all cars that are damaged or need a repair.
SELECT cVIN, cMake, cModel, cYear, pAddr, cStatus FROM car WHERE cStatus = "damaged" OR cStatus = "not_running";

prompt --Find the car with the highest/lowest number of rentals.
SELECT cvin, count, cmake, cmodel, cyear, cread, cstatus, paddr FROM (SELECT cvin, COUNT(*) as count FROM car JOIN renthistory USING(cvin) GROUP BY cVIN ORDER BY COUNT(*) DESC LIMIT 1) as Top JOIN car USING(cvin);
SELECT cvin, count, cmake, cmodel, cyear, cread, cstatus, paddr FROM (SELECT cvin, COUNT(*) as count FROM car JOIN renthistory USING(cvin) GROUP BY cVIN ORDER BY COUNT(*) ASC LIMIT 1) as Top JOIN car USING(cvin);

prompt --Show all locations
SELECT pAddr FROM PLoc;

prompt --Show all cars at a location
SELECT cVIN, cFee, cMake, cModel, cYear, pAddr FROM Car WHERE pAddr = "1011 Albert";

prompt --Show all reservation for a car, if any
SELECT reDate, reLength FROM reservation WHERE cvin = "00000000000000001";

prompt -- Show information for all cars
SELECT cvin, cmake, cmodel, cyear FROM car;

prompt -- Show information for a car
SELECT cvin, cmake, cmodel, cyear FROM car WHERE cvin = "00000000000000001";

prompt --Show reservations on a given day
SELECT cmake, cmodel, cyear, cvin, paddr, reacccode, menum FROM car JOIN reservation USING(cvin) WHERE reDate <= UNIX_TIMESTAMP("2017-04-07") AND (reDate + (reLength - 1) * 24 * 60 * 60 >= UNIX_TIMESTAMP("2017-04-07"));

prompt --Show numbers of avaliable spaces of all locations
SELECT paddr, pnumspaces - count(*) as sleft FROM ploc JOIN car USING(paddr) GROUP BY paddr;

prompt --Add a car to fleet
INSERT INTO car VALUES ("99999999999999997", 35, "Test3", "Test3", "9999", 10000, "normal", "321 Princess");

prompt --Show all rents of a member in a month
SELECT rhptime, rhdtime, cmake, cmodel, cyear, cfee, paddr FROM renthistory join car using(cvin) WHERE rhdtime IS NOT NULL AND rhdtime < UNIX_TIMESTAMP("2017-04-01") AND rhdtime >= UNIX_TIMESTAMP("2017-03-01") AND menum = 0000000222;

prompt --Show monthly fee for a member of a given month
SELECT SUM((ROUND(((rhdtime - rhptime)*1.0/(24*60*60)),2)*cfee)) AS fee FROM (SELECT * FROM `renthistory` WHERE rhdtime IS NOT NULL AND rhdtime < UNIX_TIMESTAMP("2017-04-01") AND rhdtime >= UNIX_TIMESTAMP("2017-03-01") AND menum = 0000000222) AS t1 JOIN car USING(cvin);

prompt --Show name and email address of a given member
SELECT meName, meEmail from member where menum = 0000000222;

prompt --Reply to a comment
UPDATE comment SET adminNum = 1, coReply = "Thank you." WHERE coID = 3;

prompt --Show all comment
SELECT coRating, coText, meNum, meName, cmake, cmodel, cyear, cVIN, coID, coreply FROM comment JOIN car USING(cVIN) JOIN member USING(meNum);

prompt --Show a comment
SELECT coRating, coText, member.meName, member.meemail, cmake, cmodel, cyear, cVIN, admin.meName, coreply FROM comment JOIN car USING(cvin), member AS member, member AS admin WHERE coID = 3 AND comment.meNum = member.meNum AND comment.adminNum = admin.meNum;