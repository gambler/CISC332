<!DOCTYPE HTML>
<html>
<head>
	<title>Reservation placed</title>
</head>
<body>
  <?php
    include_once '../config/auth.php';
    if($id=checkAuth()){

    } else {
      echo("no auth");
      header("Location: /index.php?logout=1");
      die();
    }
  ?>
    <?php
    include_once "../config/connect.php";
    $con = connect();
    ?>
    <?php
    printf("%s, from %s to %s, length %d\n",$_POST['vin'], date("d/m/Y", ($_POST['bday'])), date("d/m/Y", $_POST['eday']), floor(($_POST['eday']-$_POST['bday'])/(24*60*60)+1));
    ?>
    <?php if ($_POST['eday']-$_POST['bday'] < 0) { ?>
    	Bad quary. Check the date.<br />
    	<a href="index.php">redirect</a><br/> 
    	die();
    <?php }?>
    <?php
    $code = str_pad(rand(0, pow(10, 10)-1), 10, '0', STR_PAD_LEFT);
    $query = "INSERT INTO reservation (reDate, reAccCode, reLength, meNum, cVIN) VALUES (?, ?, ?, ?, ?)";
    $stmt = $con->prepare($query);
    $length = floor(($_POST['eday']-$_POST['bday'])/(24*60*60)+1);
    $stmt->bind_Param("dsdds", $_POST['bday'], $code, $length, $id, $_POST['vin']);
    $result = $stmt->execute();
    if(!$result) {
    	echo "reservation failed<br />";
    }
    else {
    	echo "reservation successfull!<br />";
    	printf("Access Code: %s<br />", $code);
    }    		
    ?>
    <a href="index.php">redirect</a><br/> 
    
</body>
</html>