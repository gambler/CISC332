<?php
function connect(){
  $host = "localhost";
  $db_name = "ktcs";
  $username = "cisc332";
  $password = "cisc332password";
  try {
      $con = new mysqli($host,$username,$password, $db_name);
      return $con;
  }
  catch(Exception $exception){
      echo "Connection error: " . $exception->getMessage();
      die();
  }
}
?>