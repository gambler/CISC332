<!doctype html>
<html lang="en">
	<head>
		<title>Welcome to KTCS</title>
		<style>
			body {font-family: "Lato", sans-serif;}
	
			/* Style the tab */
			div.tab {
				overflow: hidden;
				border: 1px solid #ccc;
				background-color: #f1f1f1;
			}
	
			/* Style the buttons inside the tab */
			div.tab button {
				background-color: inherit;
				float: left;
				border: none;
				outline: none;
				cursor: pointer;
				padding: 14px 16px;
				transition: 0.3s;
				font-size: 17px;
				}
		
			/* Change background color of buttons on hover */
			div.tab button:hover {
				background-color: #ddd;
			}
			
			/* Create an active/current tablink class */
			div.tab button.active {
				background-color: #ccc;
			}
			
			/* Style the tab content */
			.tabcontent {
				display: none;
				padding: 6px 12px;
				border: 1px solid #ccc;
				border-top: none;
			}
			
			.tabcontent {
				-webkit-animation: fadeEffect 1s;
				animation: fadeEffect 1s; /* Fading effect takes 1 second */
			}
			
			@-webkit-keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}
			
			@keyframes fadeEffect {
				from {opacity: 0;}
				to {opacity: 1;}
			}
		</style>
		<script>
		function openAction(evt, actionName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(" active", "");
			}
			document.getElementById(actionName).style.display = "block";
			evt.currentTarget.actionName += " active";
		}
		</script>
		<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>


	</head>
	<body>
		<?php $defaultTimeZone='UTC'; ?>
		<?php
		include_once '../config/auth.php';
		if($id=checkAuth()){
	
		} else {
			echo("no auth");
			header("Location: /index.php?logout=1");
			die();
		}
		?>
		HELLO! <a href="/index.php?logout=1">Log Out</a><br/>
	
		<?php
		include_once "../config/connect.php";
		$con = connect();
		?>
	
		<div class="tab">
			<?php $flag = 0;
			isset($_POST['searchLoc'])?$flag=1:null;
			isset($_POST['searchCar'])?$flag=2:null;
			isset($_POST['searchResv'])?$flag=3:null;
			isset($_POST['genInv'])?$flag=4:null;
			isset($_POST['replyComm'])?$flag=5:null;?>
			<button class="tablinks" onclick="openAction(event,'Statistics')" <?php $flag==0?printf("id=\"defaultOpen\""):null;?>>Statistics</button>
			<button class="tablinks" onclick="openAction(event, 'Locations')" <?php $flag==1?printf("id=\"defaultOpen\""):null;?>>Locations</button>
			<button class="tablinks" onclick="openAction(event, 'Fleet')" >Fleet</button>
			<button class="tablinks" onclick="openAction(event, 'Cars')" <?php $flag==2?printf("id=\"defaultOpen\""):null;?>>Cars</button>
			<button class="tablinks" onclick="openAction(event, 'Reservations')" <?php $flag==3?printf("id=\"defaultOpen\""):null;?>>Reservations</button>
			<button class="tablinks" onclick="openAction(event, 'Generate Invoice')" <?php $flag==4?printf("id=\"defaultOpen\""):null;?> >Generate Invoice</button>
			<button class="tablinks" onclick="openAction(event, 'Comments')" <?php $flag==5?printf("id=\"defaultOpen\""):null;?> >Comments</button>
		</div>
		
		<div id="Statistics" class="tabcontent">
		Maintenance Required:<br />
		<table>
			<tr>
				<th>VIN</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Address</th>
				<th>Read</th>
			</tr>
		<?php
			$query = "SELECT cVIN, cMake, cModel, cYear, pAddr, cRead FROM car WHERE cRead - 5000 > (SELECT max(maRead) FROM Car JOIN maintenance USING(CVIN) WHERE cvin = 99999999999999999 GROUP BY CVIN)";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($vin, $make, $model, $year, $addr, $read);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				
			}
			else{
				while ($stmt->fetch()) {
					printf("<tr>");
					printf("<td>%s</td>",$vin);
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%s</td>",$addr);
					printf("<td>%d</td>",$read);
					printf("</tr>");
				}
			} ?>
		</table>
		<table>
			<tr>
				<th>VIN</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Address</th>
				<th>Status</th>
			</tr>
		<?php 
			$query = "SELECT cVIN, cMake, cModel, cYear, pAddr, cStatus FROM car WHERE cStatus = \"damaged\" OR cStatus = \"not_running\"";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($vin, $make, $model, $year, $addr, $stat);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				
			}
			else{
				while ($stmt->fetch()) {
					printf("<tr>");
					printf("<td>%s</td>",$vin);
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%s</td>",$addr);
					printf("<td>%s</td>",$stat);
					printf("</tr>");
				}
			}
		?>
		</table>
		Most Rented Car:<br />
		<table>
			<tr>
				<th>VIN</th>
				<th>Count</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Address</th>
			</tr>
		<?php 
			$query = "SELECT cvin, count, cmake, cmodel, cyear, cread, cstatus, paddr FROM (SELECT cvin, COUNT(*) as count FROM car JOIN renthistory USING(cvin) GROUP BY cVIN ORDER BY COUNT(*) DESC LIMIT 1) as Top JOIN car USING(cvin)";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($vin, $count, $make, $model, $year, $read, $stat, $addr);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				
			}
			else{
				while ($stmt->fetch()) {
					printf("<tr>");
					printf("<td>%s</td>",$vin);
					printf("<td>%d</td>",$count);
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%s</td>",$addr);
					printf("</tr>");
				}
			}
		?>
		</table>
		Least Rented Car:<br />
		<table>
			<tr>
				<th>VIN</th>
				<th>Count</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Address</th>
			</tr>
		<?php 
			$query = "SELECT cvin, count, cmake, cmodel, cyear, cread, cstatus, paddr FROM (SELECT cvin, COUNT(*) as count FROM car JOIN renthistory USING(cvin) GROUP BY cVIN ORDER BY COUNT(*) ASC LIMIT 1) as Top JOIN car USING(cvin)";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($vin, $count, $make, $model, $year, $read, $stat, $addr);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				
			}
			else{
				while ($stmt->fetch()) {
					printf("<tr>");
					printf("<td>%s</td>",$vin);
					printf("<td>%d</td>",$count);
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%s</td>",$addr);
					printf("</tr>");
				}
			}
		?>
		</table>
		</div>
		
		<div id="Locations" class="tabcontent">
		
		<form name='locTb' id="locTb" action="admin.php" method='post'>
			Location:
			<select name='location' id='location'>
			<?php
			$result = $con->query("SELECT pAddr FROM PLoc");
			
			if(!$result)
				printf("<option value=\"NA\">NA</option>");
			else{
				while ($row = $result->fetch_assoc()) {
					printf("<option value=\"%s\">%s</option>",$row['pAddr'],$row['pAddr']);
				}
			}
			?>
			</select>
			<br/>
			<input type="submit" id='searchLoc' name='searchLoc' />
		</form>
		
		<?php
			if(!isset($_POST['searchLoc'])) {
				//die();
			} else {
			$query = "SELECT cVIN, cFee, cMake, cModel, cYear, pAddr FROM Car WHERE pAddr = ?";
			$stmt = $con->prepare($query);
			$stmt->bind_Param("s", $_POST['location']);
			$stmt->execute();
			$stmt->bind_result($vin, $fee, $make, $model, $year, $addr);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				printf("Your search returns no result.<br/>");
				die();
			}
			else{ ?>
		<table>
			<tr>
				<th>Select</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>Daily Fee</th>
				<th>Address</th>
			</tr>
	 		<?php
			while ($stmt->fetch()) {
				$query2 = "SELECT reDate, reLength FROM reservation WHERE cvin = ?";
				$stmt2 = $con->prepare($query2);
				$stmt2->bind_Param("s",$vin);
				$stmt2->execute();
				$stmt2->bind_result($bday, $len);
				$stmt2->store_result();
				printf("<tr>");
				if ($stmt2->num_rows() != 0) { ?>
					<td>
						<table>
							<tr><td>Reserved</td></tr>
							<?php while($stmt2->fetch()){ ?>
							<tr><td> <?php printf("From %s to %s",date("d/m/Y", $bday), date("d/m/Y", $bday+($len-1)*26*60*60)); ?> </td></tr>
							<?php } ?>
						</table>
					</td>
					<?php
				} else {
					printf("<td>Not reserved</td>");
				}
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%d</td>",$fee);
					printf("<td>%s</td>",$addr);
					printf("</tr>");
				} ?>
				</table>
			<?php
			}
		}
		?>
		</div>
		
		<div id="Cars" class="tabcontent">
		
		<form name='carTb' id="carTb" action="admin.php" method='post'>
			Car:
			<select name='car' id='car'>
			<?php
			$result = $con->query("SELECT cvin, cmake, cmodel, cyear FROM car");
			
			if(!$result)
				printf("<option value=\"NA\">NA</option>");
			else{
				while ($row = $result->fetch_assoc()) {
					printf("<option value=\"%s\">%s, %s, %s, %s</option>",$row['cvin'], $row['cmake'], $row['cmodel'], $row['cyear'], $row['cvin']);
				}
			}
			?>
			</select>
			<br/>
			<input type="submit" id='searchCar' name='searchCar' />
		</form>
		
		<?php
			if(!isset($_POST['searchCar'])) {
				//die();
			} else {
				$result = $con->query("SELECT cvin, cmake, cmodel, cyear FROM car WHERE cvin = ". $_POST['car']);
				if(!$result)
					;
				else{
					$row = $result->fetch_assoc();
					printf("Rental history for %s, %s, %s, %s", $row['cmake'], $row['cmodel'], $row['cyear'], $row['cvin']);

					$query = "SELECT rhpRead, rhpTime, rhpStatus, rhdRead, rhdTime, rhdStatus FROM car JOIN renthistory USING(cvin) WHERE rhdstatus is not NULL and cvin = ?";
					$stmt = $con->prepare($query);
					$stmt->bind_param("s", $_POST['car']);
					$stmt->execute();
					$stmt->bind_result($rhpread, $rhptime, $rhpstatus, $rhdread, $rhdtime, $rhdstatus);
					if(!$stmt->store_result() || $stmt->num_rows == 0) {
						printf("Your search returns no result.<br/>");
						//die();
					}
					else{ ?>
				<table>
					<tr>
						<th>Pick Up Date</th>
						<th>Pick Up Reading</th>
						<th>Pick Up State</th>
						<th>Drop Off Date</th>
						<th>Drop Off Reading</th>
						<th>Drop Off Status</th>
					</tr>
			 		<?php
					while ($stmt->fetch()) {
						printf("<tr>");
						printf("<td>%s</td>",date("d/m/Y", $rhptime));
						printf("<td>%d</td>",$rhpread);
						printf("<td>%s</td>",$rhpstatus);
						printf("<td>%s</td>",$rhdtime?date("d/m/Y", $rhdtime):"");
						printf("<td>%s</td>",$rhdread?$rhdread:"");
						printf("<td>%s</td>",$rhdstatus);
						printf("</tr>");
						} ?>
						</table>
					<?php
					}
				}
			}
		?>
		</div>
		
		<div id="Reservations" class="tabcontent">
		
		<form name='reservationTb' id="reservationTb" action="admin.php" method='post'>
			Date:
			<input type="date" name="day" value="<?php	if(isset($_POST['day']))
															echo $_POST['day'];
														else
															echo date('Y-m-d'); 
														?>">
			<input type="submit" id='searchResv' name='searchResv' />
		</form>
		
		<?php
			if(!isset($_POST['searchResv'])) {
				//die();
			} else {
			$day = strtotime($_POST["day"]. ' UTC');
			$query = "SELECT cmake, cmodel, cyear, cvin, paddr, reacccode, menum FROM car JOIN reservation USING(cvin) WHERE reDate <= ? AND (reDate + (reLength - 1) * 24 * 60 * 60 >= ?)";
			$stmt = $con->prepare($query);
			$stmt->bind_param("dd", $day, $day);
			$stmt->execute();
			$stmt->bind_result($make, $model, $year, $cvin, $addr, $code, $menum);
			if(!$stmt->store_result() || $stmt->num_rows == 0) {
				printf("Your search returns no result.<br/>");
				//die();
			}
			else{ ?>
		<table>
			<tr>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>VIN</th>
				<th>Address</th>
				<th>Access Code</th>
				<th>Member Number</th>
			</tr>
	 		<?php
			while ($stmt->fetch()) {
				printf("<tr>");
				printf("<td>%s</td>",$make);
				printf("<td>%s</td>",$model);
				printf("<td>%s</td>",$year);
				printf("<td>%d</td>",$cvin);
				printf("<td>%s</td>",$addr);
				printf("<td>%s</td>",$code);
				printf("<td>%s</td>",$menum);
				printf("</tr>");
				} ?>
		</table>
			<?php
			}
		}
		?>
		</div>
		
		<div id="Fleet" class="tabcontent">
		<?php
			$query = "SELECT paddr, pnumspaces - count(*) as sleft FROM ploc JOIN car USING(paddr) GROUP BY paddr";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($paddr, $space);
			$stmt->store_result();
		if ($stmt->num_rows() != 0) { ?>
		<form name='fleetTb' id="fleetTb" action="addfleet.php" method="post">
		<table>
			<tr>
				<th>Parking Location</th>
				<th>Remaining Space</th>
			</tr>
			<?php while($stmt->fetch()){ ?>
			<tr>
				<?php
				if ($space > 0)
					printf("<td><input type=\"radio\" name=\"paddr\" id=\"paddr\" value=\"%s\">%s</td>",$paddr,$paddr);
				else
					printf("<td>%s",$paddr);
				?>
				<?php printf("<td>%d</td>",$space); ?>
			</tr>
			<?php } ?>
			</table>
		        <div>
		        	<div>
						<label for="vim">VIN :</label>
						<input type="text" name="vin" id="vin" placeholder="VIN">
						<label for="make">Make :</label>
						<input type="text" name="make" id="make" placeholder="Make">
						<label for="model">Model :</label>
						<input type="text" name="model" id="model" placeholder="Model">
						<label for="year">Year :</label>
						<input type="text" name="year" id="year" placeholder="Year">
						<label for="read">Odometer Reading :</label>
						<input type="text" name="read" id="read" placeholder="Odometer Reading">
						<label for="status">Status :</label>
						<select name='status' id='status'>
							<option value="normal">normal</option>
							<option value="damaged">damaged</option>
							<option value="not_running">not running</option>
						</select>
						<label for="fee">Daily Fee :</label>
						<input type="text" name="fee" id="fee" placeholder="Daily Fee">
					</div>
		          <input type="submit" data-inline="true" value="Drop Off">
		        </div>
			
			</form>
		<?php
		} else {
			printf("No more space left");
		}
		?>
		</div>
		
		<div id="Generate Invoice" class="tabcontent">
		<form name='invoiceTb' id="invoiceTb" action="admin.php" method='post'>
			Month :
			<input type="month" name="month" id="month"/>
			Member :
			<input type="text" name="menum" id="menum" placeholder="Member Number">
			<input type="submit" id="genInv" name="genInv" value="Generate Invoice">
		</form>
			<?php
			function intdiv($a, $b) {
				return ($a - $a % $b) / $b;
			}
			
			if(!isset($_POST['genInv'])) {
				//die();
			} else {
				$month = explode("-", $_POST['month']);
				$year = intval($month[0]);
				$month = intval($month[1]) + 1;
				$year = $year + intdiv($month, 12);
				$month = $month % 12;
				$month = sprintf("%4d-%02d", $year, $month);
				$month1 = strtotime($_POST['month']. ' UTC');
				$month2 = strtotime($month. ' UTC');
				
				$str = "";
				$str .= "<table>";
				$str .= "
					<tr>
						<th>Date</th>
						<th>Make</th>
						<th>Model</th>
						<th>Year</th>
						<th>Daily Fee</th>
						<th>Address</th>
					</tr>";
				
				$query = "SELECT rhptime, rhdtime, cmake, cmodel, cyear, cfee, paddr FROM renthistory join car using(cvin) WHERE rhdtime IS NOT NULL AND rhdtime < ? AND rhdtime >= ? AND menum = ?";
				$stmt = $con->prepare($query);
				$stmt->bind_param("ddd", $month2, $month1, $_POST['menum']);
				$stmt->execute();
				$stmt->bind_result($bt, $et, $make, $model, $year, $fee, $addr);
				while($stmt->fetch()){
					$str .= "<tr>";
					$str .=sprintf("<td>From %s To %s</td>",date("Y-m-d", $bt), date("Y-m-d", strtotime(date("Y-m-d", $et). ' UTC')-24*3600));
					$str .=sprintf("<td>%s</td>",$make);
					$str .=sprintf("<td>%s</td>",$model);
					$str .=sprintf("<td>%s</td>",$year);
					$str .=sprintf("<td>%d</td>",$fee);
					$str .=sprintf("<td>%s</td>",$addr);
					$str .= "</tr>";
				}
				
				$stmt->close();
				$query = "SELECT SUM((ROUND(((rhdtime - rhptime)*1.0/(24*60*60)),2)*cfee)) AS fee FROM (SELECT * FROM `renthistory` WHERE rhdtime IS NOT NULL AND rhdtime < ? AND rhdtime >= ? AND menum = ?) AS t1 JOIN car USING(cvin)";
				$stmt = $con->prepare($query);
				$stmt->bind_param("ddd", $month2, $month1, $_POST['menum']);
				$stmt->execute();
				$stmt->bind_result($fee);
				$stmt->store_result();
				if ($stmt->num_rows() != 0) {
					$stmt->fetch();
					if($fee==null) $fee=0;
				} else {
					$fee = 0;
				}
				
				$str .= "<tr>";
				$str .=sprintf("<td><b>Total</b></td>");
				$str .=sprintf("<td></td>");
				$str .=sprintf("<td></td>");
				$str .=sprintf("<td></td>");
				$str .=sprintf("<td></td>");
				$str .=sprintf("<td><b>$%d</b></td>",$fee);
				$str .= "</tr>";
				
				$str .= "</table>";
				printf($str);
				
				$stmt->close();
				$query = "SELECT meName, meEmail from member where menum = ?";
				$stmt = $con->prepare($query);
				$stmt->bind_param("d", $_POST['menum']);
				$stmt->execute();
				$stmt->bind_result($name, $email);
				$stmt->store_result();
				if ($stmt->num_rows() != 0) {
					$stmt->fetch();
					// send email
					//mail($name ."<" . $email . ">", "test",$str, "From: noreply@ktcs.com");
				} else {
					printf("Member does not exist");
				}

			}?>
		</div>
		
		<div id="Comments" class="tabcontent">
		<?php
			if(!isset($_POST['replyComm'])) {
				//die();
			} else {
				$query = "UPDATE comment SET adminNum = ?, coReply = ? WHERE coID = ?";
				$stmt = $con->prepare($query);
				$stmt->bind_Param("dsd", $id, $_POST['reply'], $_POST['coid']);
				$result = $stmt->execute();
			}?>
		<?php
			$query = "SELECT coRating, coText, meNum, meName, cmake, cmodel, cyear, cVIN, coID, coreply FROM comment JOIN car USING(cVIN) JOIN member USING(meNum)";
			$stmt = $con->prepare($query);
			$stmt->execute();
			$stmt->bind_result($rate, $text, $menum, $meName, $make, $model, $year, $cvin, $coid, $coreply);
			$stmt->store_result();
		if ($stmt->num_rows() != 0) { ?>
		<form name='commentTb' id="commentTb" action="admin.php" method="post">
		<table>
			<tr>
				<th>Select</th>
				<th>Rating</th>
				<th>Comment</th>
				<th>Make</th>
				<th>Model</th>
				<th>Year</th>
				<th>VIN</th>
				<th>Reply</th>
			</tr>
			<?php while($stmt->fetch()){ ?>
			<tr>
				<?php
				if ($coreply == null)
					printf("<td><input type=\"radio\" name=\"coid\" id=\"coid\" value=\"%d\">%d</td>",$coid,$coid);
				else
					printf("<td>%s</td>",$coid);
				?>
				<?php 
					printf("<td>%s</td>",$rate); 
					printf("<td>%s</td>",$text);
					printf("<td>%s</td>",$make);
					printf("<td>%s</td>",$model);
					printf("<td>%s</td>",$year);
					printf("<td>%s</td>",$cvin);
					printf("<td>%s</td>",$coreply?$coreply:"");
					
				?>
			</tr>
			<?php } ?>
			</table>
		        <div>
		        	<div>
						<label for="reply">Reply :</label>
						<input type="text" name="reply" id="reply" placeholder="Reply">
					</div>
		          <input type="submit" name="replyComm" id="replyComm" value="Reply">
		        </div>
		        
		    <?php
			if(!isset($_POST['replyComm'])) {
				//die();
			} else {
				$query = "SELECT coRating, coText, member.meName, member.meemail, cmake, cmodel, cyear, cVIN, admin.meName, coreply FROM comment JOIN car USING(cvin), member AS member, member AS admin WHERE coID = ? AND comment.meNum = member.meNum AND comment.adminNum = admin.meNum";
				$stmt = $con->prepare($query);
				$stmt->bind_param("d", $_POST['coid']);
				$stmt->execute();
				$stmt->bind_result($rate, $text, $meName, $email, $make, $model, $year, $cvin, $adminName, $coreply);
				$stmt->fetch();
				
				$str = "";
				$str .= "<p>Dear";
				$str .= $meName . ":</p>";
				
				$str .= "<p>Thank you for riding with us!</p>";
				
				$str .= "<p>" . $coreply . "</p>";
				
				$str .= "<p>We hope to see you again!</p>";
				
				$str .= "<p>Yours,<br />";
				$str .= $adminName . "</p>";
				
				printf($str);
				
				// send email
				//mail($name ."<" . $email . ">", "test",$str, "From: noreply@ktcs.com");
			}?>
			
			</form>
		<?php
		} else {
			printf("No Comment");
		}
		?>
		</div>
		
		<script>
		// Get the element with id="defaultOpen" and click on it
		document.getElementById("defaultOpen").click();
		</script>
	</body>
</html>
