<!doctype html>
<html lang="en">
  <head>
    <title>Sign up to KTCS</title>
  </head>
  <body>
  
  <?php
    $legal = true;
    
    echo("
      <h1>Please Fill in Your Information</h1>
    
      <form action=\"signup.php\" method= \"post\">
        <p>
          Name:<br />
          <input type=\"text\" id=\"name\" name=\"name\" size=\"20\" maxlength=\"50\"
    ");
    
    if(isset($_POST['name']))
      printf(" value=\"%s\"",$_POST['name']);
    else
      $legal = false;
    
    echo("/>");
    
    // This might be too strict
    // ^[[:alpha:]]+(\s[[:alpha:]]+)*$
    if(!empty($_POST['name']) && preg_match("/[^\s[:alpha:]]/",$_POST['name'])) {
      printf("Can only cotain letters, seperated by space");
      $legal = false;
    }
    
    echo("
        </p>
        <p>
          Address:<br />
          <input type=\"text\" id=\"addr\" name=\"addr\" size=\"20\" maxlength=\"150\"
    ");
    
    if(isset($_POST['addr']))
      printf(" value=\"%s\"",$_POST['addr']);
    else
      $legal = false;
    
    echo("/>");

    echo("
        </p>
        <p>
          Phone:<br />
          <input type=\"text\" id=\"phone\" name=\"phone\" size=\"20\" maxlength=\"10\"
    ");
    
    if(isset($_POST['phone']))
      printf(" value=\"%s\"",$_POST['phone']);
    else
      $legal = false;
    
    echo("/>");
    
    if(!empty($_POST['phone']) && !preg_match("/^[0-9]{10}$/",$_POST['phone'])) {
      printf("must be 10 digits");
      $legal = false;
    }
    
    echo("
        </p>
        <p>
          Email:<br />
          <input type=\"text\" id=\"email\" name=\"email\" size=\"20\" maxlength=\"40\"
    ");
    
    if(isset($_POST['email']))
      printf(" value=\"%s\"",$_POST['email']);
    else
      $legal = false;
    
    echo("/>");
    
    if(!empty($_POST['email']) && !preg_match("/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",$_POST['email'])) {
      printf("please check the format");
      $legal = false;
    }
    
    echo("
        </p>
        <p>
          Password:<br />
          <input type=\"text\" id=\"password\" name=\"password\" size=\"20\" maxlength=\"140\"
    ");
    
    if(!empty($_POST['password'])) {
      if (strlen($_POST['password'])<6 || strlen($_POST['password'])>30) {
        echo("/>");
        printf("Password must contain 6-30 characters");
        $legal = false;
      }
      else {
        printf(" value=\"%s\"",$_POST['password']);
      }
    }
    else {
      echo("/>");
    }
                
    echo("
        </p>
        <p>
          Repeat Password<br />
          <input type=\"text\" id=\"rpassword\" name=\"rpassword\" size=\"20\" maxlength=\"40\"
    ");

    if(isset($_POST['rpassword']))
      printf(" value=\"%s\"",$_POST['rpassword']);
    else
      $legal = false;
    
    echo("/>");
    
    if(isset($_POST['rpassword']) && isset($_POST['password']))
      if($_POST['rpassword'] != $_POST['password']) {
        printf("Password has to match");
        $legal = false;
      }
    
    echo("
        </p>
        <p>
          Driver's License Number:<br />
          <input type=\"text\" id=\"lnum\" name=\"lnum\" size=\"20\" maxlength=\"17\"
    ");
    
    if(isset($_POST['lnum']))
      printf(" value=\"%s\"",$_POST['lnum']);
    else
      $legal = false;
    
    echo("/>");
    
    if(!empty($_POST['lnum']) && !preg_match("/^[a-zA-Z][0-9]{4}-?[0-9]{5}-?[0-9]{5}$/",$_POST['lnum'])) {
      printf("Check the format. It must be an Ontario Driver's License");
      $legal = false;
    }
    
    echo("
        </p>

        <input type= \"submit\" id=\"submit\" name = \"submit\" value=\"Go!\" />
      ");
      
      if ($legal) {
        include_once '../config/connect.php';
        $con = connect();

        if(isset($_GET['logout'])){
          $sel_q = "DELETE FROM Session WHERE sID=?";
          $stmt = $con->prepare($sel_q);
          $stmt->bind_param('s', $_COOKIE["ktcs_auth"]);
          $stmt->execute();
          unset($_COOKIE["ktcs_auth"]);
        }
        
        $ins_q = "INSERT INTO member (meName, meAddr, mePhone, meEmail, meLicense, mePw, meFee) VALUES(\"%s\", \"%s\", \"%s\", \"%s\", \"%s\", \"%s\", 20)";
        
        $query = sprintf($ins_q,$_POST['name'],$_POST['addr'],$_POST['phone'],$_POST['email'],$_POST['lnum'],md5($_POST['password']));
        
        printf("<p>");
        $result = $con->query($query);
        if(!$result)
          printf('Failed: %s', $con->error);
        else
          printf("Successfull! Your member ID is: %010d", $con->query("SELECT LAST_INSERT_ID() as id")->fetch_object()->id);
        printf("</>");
      }
      
    ?>
  </body>
</html>
