/* 
	CISC 332 Project 
	Version 2.1
    Author: Bochen Yan, Yunshan (Richard) Yan
    StudentID: 10142714, 10137700
    
    Last Edit: 2017/2/28
*/

USE ktcs;

SET sql_mode = 'STRICT_ALL_TABLES';

SET FOREIGN_KEY_CHECKS=0;

  SET @tables = NULL;
  SET GROUP_CONCAT_MAX_LEN=32768;

  SELECT GROUP_CONCAT('`', table_schema, '`.`', table_name, '`') INTO @tables
  FROM   information_schema.tables 
  WHERE  table_schema = (SELECT DATABASE());
  SELECT IFNULL(@tables, '') INTO @tables;

  SET        @tables = CONCAT('DROP TABLE IF EXISTS ', @tables);
  PREPARE    stmt FROM @tables;
  EXECUTE    stmt;
  DEALLOCATE PREPARE stmt;

CREATE TABLE PLoc
(
  pAddr VARCHAR(140) NOT NULL,
  pNumSpaces INT NOT NULL,
  PRIMARY KEY (pAddr)
);

CREATE TABLE Car
(
  cVIN CHAR(17) NOT NULL,
  cFee INT NOT NULL,
  cMake VARCHAR(40),
  cModel VARCHAR(40),
  cYear CHAR(4),
  cRead INT NOT NULL,
  cStatus ENUM('normal', 'damaged', 'not_running') NOT NULL,
  pAddr VARCHAR(140) NOT NULL,
  PRIMARY KEY (cVIN),
  FOREIGN KEY (pAddr) REFERENCES PLoc(pAddr)
);

CREATE TABLE Maintenance
(
  maDate INT UNSIGNED NOT NULL,
  maRead INT NOT NULL,
  maType ENUM('scheduled', 'repair', 'body_work') NOT NULL,
  maDesc VARCHAR(140),
  maID INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  cVIN CHAR(17) NOT NULL,
  PRIMARY KEY (maID),
  FOREIGN KEY (cVIN) REFERENCES Car(cVIN)
);

CREATE TABLE Member
(
  meNum INT(10) UNSIGNED ZEROFILL auto_increment NOT NULL,
  meName VARCHAR(50) NOT NULL,
  meAddr VARCHAR(140) , # NOT NULL, inforced at business logic level
  mePhone CHAR(10) , # NOT NULL,
  meEmail VARCHAR(40) NOT NULL,
  meLicense CHAR(15) , # NOT NULL,
  meFee INT DEFAULT 20,
  mePw CHAR(32) NOT NULL,
  ifAdmin BOOLEAN NOT NULL DEFAULT FALSE,
  PRIMARY KEY (meNum)
);

CREATE TABLE Session
(
  sID CHAR(32) NOT NULL,
  sExpTime datetime NOT NULL,
  meNum INT(10) UNSIGNED ZEROFILL NOT NULL,
  PRIMARY KEY (sID),
  FOREIGN KEY (meNum) REFERENCES Member(meNum)
);

CREATE TABLE Comment
(
  coRating ENUM('1', '2', '3', '4') NOT NULL,
  coText VARCHAR(140) NOT NULL,
  meNum INT(10) UNSIGNED ZEROFILL NOT NULL,
  cVIN CHAR(17) NOT NULL,
  coID INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  adminNum INT(10) UNSIGNED,
  coReply VARCHAR(140),
  PRIMARY KEY (coID),
  FOREIGN KEY (meNum) REFERENCES Member(meNum),
  FOREIGN KEY (adminNum) REFERENCES Member(meNum),
  FOREIGN KEY (cVIN) REFERENCES Car(cVIN)
);

CREATE TABLE RentHistory
(
  rhPRead INT NOT NULL,
  rhPTime INT UNSIGNED NOT NULL,
  rhPStatus ENUM('normal', 'damaged', 'not_running') NOT NULL,
  rhDRead INT,
  rhDTime INT UNSIGNED,
  rhDStatus ENUM('normal', 'damaged', 'not_running'),
  meNum INT(10) UNSIGNED ZEROFILL NOT NULL,
  cVIN CHAR(17) NOT NULL,
  rhID INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (rhID),
  FOREIGN KEY (meNum) REFERENCES Member(meNum),
  FOREIGN KEY (cVIN) REFERENCES Car(cVIN)
);

CREATE TABLE Reservation
(
  reDate INT UNSIGNED NOT NULL,
  reAccCode CHAR(10) NOT NULL,
  reLength INT NOT NULL,
  meNum INT(10) UNSIGNED ZEROFILL NOT NULL,
  cVIN CHAR(17) NOT NULL,
  reID INT UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (reID),
  FOREIGN KEY (meNum) REFERENCES Member(meNum),
  FOREIGN KEY (cVIN) REFERENCES Car(cVIN)
);

SET FOREIGN_KEY_CHECKS=1;

DROP EVENT IF EXISTS expSess;

CREATE EVENT expSess
  ON SCHEDULE EVERY 10 MINUTE
  DO DELETE FROM session WHERE UNIX_TIMESTAMP(sExpTime)<UNIX_TIMESTAMP(now());

SET GLOBAL event_scheduler = ON;

DROP EVENT IF EXISTS expRes;

CREATE EVENT expRes
  ON SCHEDULE EVERY 1 Day
  DO DELETE FROM reservation WHERE reDate + reLength * 24*60*60 > UNIX_TIMESTAMP(now());

SET GLOBAL event_scheduler = ON;

insert into PLoc values
("1011 Albert", 12), 
("123 Johnson", 20), 
("321 Princess", 15);
select * from PLoc;

insert into Car values
("00000000000000001", 30, "Honda", "Civic", "2016", 0, "normal", "1011 Albert"),
("00000000000000002", 40, "Audi", "A3", "2014", 0, "normal", "123 Johnson"), 
("00000000000000003", 35, "Ford", "Explorer", "2013", 0, "normal", "321 Princess"),
("99999999999999998", 35, "Test2", "Test2", "9999", 0, "damaged", "321 Princess"),
("99999999999999999", 35, "Test", "Test", "9999", 10000, "normal", "321 Princess");
select * from Car;

insert into Maintenance values
(1487894400, 5014, "scheduled", "Yearly check", 245, "00000000000000001"),
(1487980800, 7769, "repair", null, 246, "00000000000000002"),
(1488067200, 10530, "body_work", null, 247, "00000000000000003"),
(1487894400, 1023, "scheduled", "Yearly check", 248, "99999999999999999"),
(1487994400, 4099, "scheduled", "Yearly check", 249, "99999999999999999");
select * from Maintenance;

insert into Member values
(0000000001, "Admin", null, null, "admin@ktcs.com", null, 0, md5("admin"), true),
(0000000111, "Bochen Yan", "2xx Colborne", "3433641234", "kerwinyan@hotmail.com", "125643549620202", 20, md5("hello"), false),
(0000000222, "Yunshan Yan", "4xx Princess", "3433647896", "yunshanYan@hotmail.com", "456897546923145", 20, md5("world"), false);

insert into Member (meName, meAddr, mePhone, meEmail, meLicense, mePw, meFee) values
("John Doe", "Nowhere", "0000000000", "nobody@nowhere.com", "A00000000000000", md5("let me pass"), 20);
SELECT LAST_INSERT_ID();

select * from Member;

select * from Session;

insert into Comment (coRating, coText, meNum, cVIN) values
(2, "Not so good", 0000000222, "00000000000000002");
insert into Comment (coRating, coText, meNum, cVIN, adminNum, coReply) values
(4, "Pretty good", 0000000111, "00000000000000001", 0000000001, "Glad you liked it");
select * from Comment;
  
insert into RentHistory (rhPRead, rhPTime, rhPStatus, rhDRead, rhDTime, rhDStatus, meNum, cVIN) values
(4950, 1487548800, "normal", 5000, 1487635200, "normal", 0000000111, "00000000000000001");

insert into RentHistory (rhPRead, rhPTime, rhPStatus, rhDRead, rhDTime, rhDStatus, meNum, cVIN) values
(4950, 1489449600, "normal", 5000, 1489535999, "normal", 0000000222, "00000000000000001"),
(4950, 1489622400, "normal", 5000, 1489795199, "normal", 0000000222, "00000000000000002");
select * from RentHistory;

insert into Reservation (reDate, reAccCode, reLength, meNum, cVIN) values
(UNIX_TIMESTAMP("2017-04-07"), "1234567890", 1, 0000000111, "00000000000000001"),
(UNIX_TIMESTAMP("2017-04-08"), "9876543210", 2, 0000000223, "00000000000000001");
select * from Reservation;
