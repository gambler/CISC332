<!DOCTYPE HTML>
<html>
<head>
	<title>Reservation placed</title>
</head>
<body>
  <?php
    include_once '../config/auth.php';
    if($id=checkAuth()){

    } else {
      echo("no auth");
      header("Location: /index.php?logout=1");
      die();
    }
  ?>
    <?php
    include_once "../config/connect.php";
    $con = connect();
    ?>

    <?php
    $query = "SELECT cVIN FROM reservation WHERE reid = ?";
    $stmt = $con->prepare($query);
    $stmt->bind_Param("d", $_POST['reid']);
    $result = $stmt->execute();
    $stmt->bind_result($cvin);
    if(!$result) {
    	echo "pickup failed<br />";
    }
    else {
    	$stmt->fetch();
    	$stmt->close();
	    $query = "INSERT INTO RentHistory (rhPRead, rhPTime, rhPStatus, meNum, cVIN) VALUES (?, ?, ?, ?, ?)";
	    $stmt = $con->prepare($query);
	    $timeNow = time();
	    $stmt->bind_Param("ddsds", $_POST['oread'], $timeNow, $_POST['status'], $id, $cvin);
	    $result = $stmt->execute();
	    /*if (!$statement->execute()) {
	    	trigger_error('Error executing MySQL query: ' . $statement->error);
	    }*/
	    if(!$result) {
	    	echo "pickup failed<br />";
	    }
	    else {
	    	echo "pickup successfull!<br />";
	    	$query = "DELETE FROM reservation WHERE reid = ?";
	    	$stmt = $con->prepare($query);
	    	$stmt->bind_Param("d", $_POST['reid']);
	    	$result = $stmt->execute();
	    }    
    }
    ?>
    <a href="index.php">redirect</a><br/> 
    
</body>
</html>