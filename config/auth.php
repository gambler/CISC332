<?php  
  function checkAuth() {
    if (!isset($_COOKIE["ktcs_auth"])) {
      echo("not logged in<br />");
      return false;
    }
    else {
      include_once 'connect.php';
      $con = connect();
      $sID = $_COOKIE["ktcs_auth"];
      echo($_COOKIE["ktcs_auth"]);
      echo("<br />");
      $sel_q = "SELECT meNum, UNIX_TIMESTAMP(sExpTime) as expTime FROM Session WHERE sID=?";
      $stmt = $con->prepare($sel_q);
      $stmt->bind_param('s', $sID);
      $stmt->bind_result($memID, $etime);
      $stmt->execute();
      $stmt->store_result();
      if($stmt->num_rows==0) {
        echo("not logged in: cookie not exists<br />");
        return false;
      }
      else if($stmt->fetch() && $etime<=time()){
        echo("not logged in: cookie expired<br />");
        return false;
      }
      else {
        echo("logged in: cookie ok<br />");
        return $memID;
      }
    }
  }  
?>

<?php  
  function checkAdmin() {
  	$query = "SELECT ifAdmin FROM member WHERE meNum=?";
  	
  	$stmt = $con->prepare($query);
  	$stmt->bind_Param("s", $_POST['account']);
  	$stmt->execute();
  	$result = $stmt->get_result();
  	
  	if(!$result) return false;
  	else return $result->fetch_object()->ifAdmin;
  }  
?>