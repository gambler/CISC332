<!DOCTYPE HTML>
<html>
<head>
	<title>Drop Off</title>
</head>
<body>
  <?php
    include_once '../config/auth.php';
    if($id=checkAuth()){

    } else {
      echo("no auth");
      header("Location: /index.php?logout=1");
      die();
    }
  ?>
    <?php
    include_once "../config/connect.php";
    $con = connect();
    ?>
    
    <?php
    $query = "SELECT cvin FROM renthistory WHERE rhID = ?";
    $stmt = $con->prepare($query);
    $stmt->bind_Param("d", $_POST['rhid']);
    $stmt->bind_result($cvin);
    $result = $stmt->execute();
    if(!$result) {
    	echo "drop failed<br />";
    }
    else {
    	$stmt->fetch();
    	$stmt->close();
    }
    ?>

    <?php
    $query = "UPDATE RentHistory SET rhdRead = ?, rhdTime = ?, rhdStatus = ? WHERE rhID = ?";
    $stmt = $con->prepare($query);
    $timeNow = time();
    $stmt->bind_Param("ddsd", $_POST['oread'], $timeNow, $_POST['status'], $_POST['rhid']);
    $result = $stmt->execute();
    if(!$result) {
    	echo "dropoff failed<br />";
    }
    else {
    	$stmt->close();
    	$query = "UPDATE car SET cRead = ? WHERE cvin = ?";
    	$stmt = $con->prepare($query);
    	$stmt->bind_Param("ds", $_POST['oread'], $cvin);
    	$result = $stmt->execute();
    	if(!$result) {
    		echo "dropoff failed<br />";
    	}
    	else {
    		echo "dropoff successfull!<br />";
    	}
    }
    ?>
    
    <?php
	$query3 = "INSERT INTO comment (coRating, coText, meNum, cVIN) VALUES (?, ?, ?, ?)";
	$stmt3 = $con->prepare($query3);
	$stmt3->bind_Param("ssds", $_POST['rating'], $_POST['comment'], $id, $cvin);
	$result3 = $stmt3->execute();
	if(!$result) {
		echo "comment failed<br />";
	}
	else {
		echo "comment successfull!<br />";
	}
    ?>
    <a href="index.php">redirect</a><br/> 
    
</body>
</html>